package com.masteringextjs.masteringextjs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MasteringExtjsApplication {

	public static void main(String[] args) {
		SpringApplication.run(MasteringExtjsApplication.class, args);
	}
}
